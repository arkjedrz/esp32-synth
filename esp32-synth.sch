EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:AMS1117-3.3 U?
U 1 1 5E6507E7
P 2400 1200
F 0 "U?" H 2400 1442 50  0000 C CNN
F 1 "AMS1117-3.3" H 2400 1351 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 2400 1400 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 2500 950 50  0001 C CNN
	1    2400 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6419C6
P 1950 1450
F 0 "C?" H 2065 1496 50  0000 L CNN
F 1 "100n" H 2065 1405 50  0000 L CNN
F 2 "" H 1988 1300 50  0001 C CNN
F 3 "~" H 1950 1450 50  0001 C CNN
	1    1950 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E642D0F
P 1950 1050
F 0 "#PWR?" H 1950 900 50  0001 C CNN
F 1 "+5V" H 1965 1223 50  0000 C CNN
F 2 "" H 1950 1050 50  0001 C CNN
F 3 "" H 1950 1050 50  0001 C CNN
	1    1950 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1200 1950 1200
Wire Wire Line
	1950 1200 1950 1300
Wire Wire Line
	1950 1050 1950 1200
Connection ~ 1950 1200
$Comp
L power:GND #PWR?
U 1 1 5E643B9C
P 1950 1650
F 0 "#PWR?" H 1950 1400 50  0001 C CNN
F 1 "GND" H 1955 1477 50  0000 C CNN
F 2 "" H 1950 1650 50  0001 C CNN
F 3 "" H 1950 1650 50  0001 C CNN
	1    1950 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 1650 1950 1600
$Comp
L power:GND #PWR?
U 1 1 5E6445E1
P 2400 1650
F 0 "#PWR?" H 2400 1400 50  0001 C CNN
F 1 "GND" H 2405 1477 50  0000 C CNN
F 2 "" H 2400 1650 50  0001 C CNN
F 3 "" H 2400 1650 50  0001 C CNN
	1    2400 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 1650 2400 1500
$Comp
L power:+3.3V #PWR?
U 1 1 5E64519C
P 2850 1050
F 0 "#PWR?" H 2850 900 50  0001 C CNN
F 1 "+3.3V" H 2865 1223 50  0000 C CNN
F 2 "" H 2850 1050 50  0001 C CNN
F 3 "" H 2850 1050 50  0001 C CNN
	1    2850 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1200 2850 1200
Wire Wire Line
	2850 1200 2850 1050
$Comp
L power:GND #PWR?
U 1 1 5E6463D5
P 2850 1650
F 0 "#PWR?" H 2850 1400 50  0001 C CNN
F 1 "GND" H 2855 1477 50  0000 C CNN
F 2 "" H 2850 1650 50  0001 C CNN
F 3 "" H 2850 1650 50  0001 C CNN
	1    2850 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E646B13
P 2850 1450
F 0 "C?" H 2965 1496 50  0000 L CNN
F 1 "100n" H 2965 1405 50  0000 L CNN
F 2 "" H 2888 1300 50  0001 C CNN
F 3 "~" H 2850 1450 50  0001 C CNN
	1    2850 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1200 2850 1300
Connection ~ 2850 1200
Wire Wire Line
	2850 1600 2850 1650
Text Notes 9150 1100 0    50   ~ 0
audio out
Wire Wire Line
	9000 1500 9000 1400
$Comp
L RF_Module:ESP32-WROOM-32 U?
U 1 1 5E66E7F7
P 5250 4000
F 0 "U?" H 5250 5581 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 5250 5490 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 5250 2500 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 4950 4050 50  0001 C CNN
	1    5250 4000
	1    0    0    -1  
$EndComp
Text Notes 1800 650  0    50   ~ 0
5v<>3.3v converter
Text Notes 4450 2000 0    50   ~ 0
mcu
$Comp
L power:+3.3V #PWR?
U 1 1 5E6BBF0E
P 5250 2200
F 0 "#PWR?" H 5250 2050 50  0001 C CNN
F 1 "+3.3V" H 5265 2373 50  0000 C CNN
F 2 "" H 5250 2200 50  0001 C CNN
F 3 "" H 5250 2200 50  0001 C CNN
	1    5250 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E6BDD3C
P 5250 5450
F 0 "#PWR?" H 5250 5200 50  0001 C CNN
F 1 "GND" H 5255 5277 50  0000 C CNN
F 2 "" H 5250 5450 50  0001 C CNN
F 3 "" H 5250 5450 50  0001 C CNN
	1    5250 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 5450 5250 5400
$Comp
L power:GND #PWR?
U 1 1 5E6F2317
P 5650 2250
F 0 "#PWR?" H 5650 2000 50  0001 C CNN
F 1 "GND" V 5655 2122 50  0000 R CNN
F 2 "" H 5650 2250 50  0001 C CNN
F 3 "" H 5650 2250 50  0001 C CNN
	1    5650 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9300 1500 9400 1500
Wire Wire Line
	9400 1400 9000 1400
Connection ~ 9000 1400
Text Notes 4300 5850 0    50   ~ 0
programmer
Text Notes 600  650  0    50   ~ 0
input usb
Connection ~ 1300 1100
Wire Wire Line
	1300 1600 1300 1100
Wire Wire Line
	1300 1950 1300 1900
$Comp
L power:GND #PWR?
U 1 1 5E63E459
P 1300 1950
F 0 "#PWR?" H 1300 1700 50  0001 C CNN
F 1 "GND" H 1305 1777 50  0000 C CNN
F 2 "" H 1300 1950 50  0001 C CNN
F 3 "" H 1300 1950 50  0001 C CNN
	1    1300 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E63DA5E
P 1300 1750
F 0 "C?" H 1415 1796 50  0000 L CNN
F 1 "100n" H 1415 1705 50  0000 L CNN
F 2 "" H 1338 1600 50  0001 C CNN
F 3 "~" H 1300 1750 50  0001 C CNN
	1    1300 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 1100 1300 1000
Wire Wire Line
	1200 1100 1300 1100
$Comp
L power:+5V #PWR?
U 1 1 5E63CEEE
P 1300 1000
F 0 "#PWR?" H 1300 850 50  0001 C CNN
F 1 "+5V" H 1315 1173 50  0000 C CNN
F 2 "" H 1300 1000 50  0001 C CNN
F 3 "" H 1300 1000 50  0001 C CNN
	1    1300 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  1800 900  1700
Connection ~ 900  1800
Wire Wire Line
	800  1800 900  1800
Wire Wire Line
	800  1700 800  1800
Wire Wire Line
	900  1950 900  1800
$Comp
L power:GND #PWR?
U 1 1 5E63AC55
P 900 1950
F 0 "#PWR?" H 900 1700 50  0001 C CNN
F 1 "GND" H 905 1777 50  0000 C CNN
F 2 "" H 900 1950 50  0001 C CNN
F 3 "" H 900 1950 50  0001 C CNN
	1    900  1950
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J?
U 1 1 5E642041
P 900 1300
F 0 "J?" H 957 1767 50  0000 C CNN
F 1 "USB_B_Micro" H 957 1676 50  0000 C CNN
F 2 "" H 1050 1250 50  0001 C CNN
F 3 "~" H 1050 1250 50  0001 C CNN
	1    900  1300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5E69F9F4
P 5000 6000
F 0 "J?" H 5080 5992 50  0000 L CNN
F 1 "BOOT OPTION" H 5080 5901 50  0000 L CNN
F 2 "" H 5000 6000 50  0001 C CNN
F 3 "~" H 5000 6000 50  0001 C CNN
	1    5000 6000
	1    0    0    -1  
$EndComp
Text Label 4600 6000 0    50   ~ 0
IO0
Wire Wire Line
	4800 6100 4700 6100
$Comp
L power:GND #PWR?
U 1 1 5E6AC6A6
P 4700 6100
F 0 "#PWR?" H 4700 5850 50  0001 C CNN
F 1 "GND" V 4705 5972 50  0000 R CNN
F 2 "" H 4700 6100 50  0001 C CNN
F 3 "" H 4700 6100 50  0001 C CNN
	1    4700 6100
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 6000 4800 6000
Wire Wire Line
	4800 6450 4600 6450
Text Label 4600 6450 0    50   ~ 0
TXD
Text Label 4600 6550 0    50   ~ 0
RXD
Wire Wire Line
	4600 6550 4800 6550
$Comp
L power:GND #PWR?
U 1 1 5E6B24CB
P 4700 6650
F 0 "#PWR?" H 4700 6400 50  0001 C CNN
F 1 "GND" V 4705 6522 50  0000 R CNN
F 2 "" H 4700 6650 50  0001 C CNN
F 3 "" H 4700 6650 50  0001 C CNN
	1    4700 6650
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 6650 4800 6650
Text Label 5950 2800 0    50   ~ 0
IO0
Wire Wire Line
	5950 2800 5850 2800
Wire Wire Line
	5850 3100 5950 3100
Text Label 6350 2900 0    50   ~ 0
TXD
Text Label 5950 3100 0    50   ~ 0
RXD
Text Notes 700  4600 0    50   ~ 0
osc1 type button
$Comp
L power:+3.3V #PWR?
U 1 1 5E6D0798
P 800 4850
F 0 "#PWR?" H 800 4700 50  0001 C CNN
F 1 "+3.3V" H 815 5023 50  0000 C CNN
F 2 "" H 800 4850 50  0001 C CNN
F 3 "" H 800 4850 50  0001 C CNN
	1    800  4850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 5E6D07A2
P 800 5100
F 0 "SW?" V 754 5198 50  0000 L CNN
F 1 "SW_SPST" V 845 5198 50  0000 L CNN
F 2 "" H 800 5100 50  0001 C CNN
F 3 "~" H 800 5100 50  0001 C CNN
	1    800  5100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6D07AC
P 800 5600
F 0 "R?" H 870 5646 50  0000 L CNN
F 1 "100k" H 870 5555 50  0000 L CNN
F 2 "" V 730 5600 50  0001 C CNN
F 3 "~" H 800 5600 50  0001 C CNN
	1    800  5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  5300 800  5350
$Comp
L Device:C C?
U 1 1 5E6D07B7
P 1150 5600
F 0 "C?" H 1265 5646 50  0000 L CNN
F 1 "100n" H 1265 5555 50  0000 L CNN
F 2 "" H 1188 5450 50  0001 C CNN
F 3 "~" H 1150 5600 50  0001 C CNN
	1    1150 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 5450 1150 5350
Wire Wire Line
	1150 5350 800  5350
Connection ~ 800  5350
Wire Wire Line
	800  5350 800  5450
$Comp
L power:GND #PWR?
U 1 1 5E6D07C5
P 800 5800
F 0 "#PWR?" H 800 5550 50  0001 C CNN
F 1 "GND" H 805 5627 50  0000 C CNN
F 2 "" H 800 5800 50  0001 C CNN
F 3 "" H 800 5800 50  0001 C CNN
	1    800  5800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E6D07CF
P 1150 5800
F 0 "#PWR?" H 1150 5550 50  0001 C CNN
F 1 "GND" H 1155 5627 50  0000 C CNN
F 2 "" H 1150 5800 50  0001 C CNN
F 3 "" H 1150 5800 50  0001 C CNN
	1    1150 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 5800 1150 5750
Wire Wire Line
	800  5800 800  5750
Wire Wire Line
	800  4900 800  4850
Wire Wire Line
	1150 5350 1450 5350
Connection ~ 1150 5350
Text Label 1450 5350 0    50   ~ 0
OSC1_BUTTON
Text Notes 750  6150 0    50   ~ 0
osc2 type button
$Comp
L power:+3.3V #PWR?
U 1 1 5E6D6A77
P 850 6400
F 0 "#PWR?" H 850 6250 50  0001 C CNN
F 1 "+3.3V" H 865 6573 50  0000 C CNN
F 2 "" H 850 6400 50  0001 C CNN
F 3 "" H 850 6400 50  0001 C CNN
	1    850  6400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 5E6D6A81
P 850 6650
F 0 "SW?" V 804 6748 50  0000 L CNN
F 1 "SW_SPST" V 895 6748 50  0000 L CNN
F 2 "" H 850 6650 50  0001 C CNN
F 3 "~" H 850 6650 50  0001 C CNN
	1    850  6650
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6D6A8B
P 850 7150
F 0 "R?" H 920 7196 50  0000 L CNN
F 1 "100k" H 920 7105 50  0000 L CNN
F 2 "" V 780 7150 50  0001 C CNN
F 3 "~" H 850 7150 50  0001 C CNN
	1    850  7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  6850 850  6900
Wire Wire Line
	1200 7000 1200 6900
Wire Wire Line
	1200 6900 850  6900
Connection ~ 850  6900
Wire Wire Line
	850  6900 850  7000
$Comp
L power:GND #PWR?
U 1 1 5E6D6AA4
P 850 7350
F 0 "#PWR?" H 850 7100 50  0001 C CNN
F 1 "GND" H 855 7177 50  0000 C CNN
F 2 "" H 850 7350 50  0001 C CNN
F 3 "" H 850 7350 50  0001 C CNN
	1    850  7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  7350 850  7300
Wire Wire Line
	850  6450 850  6400
$Comp
L Amplifier_Operational:LM358 U?
U 1 1 5E6DA9F9
P 1900 3950
F 0 "U?" H 1900 4317 50  0000 C CNN
F 1 "LM358" H 1900 4226 50  0000 C CNN
F 2 "" H 1900 3950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 1900 3950 50  0001 C CNN
	1    1900 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3850 1500 3850
$Comp
L Device:R R?
U 1 1 5E6DEE8B
P 1300 3850
F 0 "R?" V 1093 3850 50  0000 C CNN
F 1 "22k" V 1184 3850 50  0000 C CNN
F 2 "" V 1230 3850 50  0001 C CNN
F 3 "~" H 1300 3850 50  0001 C CNN
	1    1300 3850
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6DFEEB
P 1300 3550
F 0 "R?" V 1093 3550 50  0000 C CNN
F 1 "12k" V 1184 3550 50  0000 C CNN
F 2 "" V 1230 3550 50  0001 C CNN
F 3 "~" H 1300 3550 50  0001 C CNN
	1    1300 3550
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6E08CB
P 1300 4150
F 0 "R?" V 1093 4150 50  0000 C CNN
F 1 "12k" V 1184 4150 50  0000 C CNN
F 2 "" V 1230 4150 50  0001 C CNN
F 3 "~" H 1300 4150 50  0001 C CNN
	1    1300 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6E11FB
P 2000 4300
F 0 "R?" V 1793 4300 50  0000 C CNN
F 1 "1.8k" V 1884 4300 50  0000 C CNN
F 2 "" V 1930 4300 50  0001 C CNN
F 3 "~" H 2000 4300 50  0001 C CNN
	1    2000 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 4050 1600 4150
Wire Wire Line
	1600 4150 1450 4150
$Comp
L power:GND #PWR?
U 1 1 5E6E8780
P 1050 4150
F 0 "#PWR?" H 1050 3900 50  0001 C CNN
F 1 "GND" V 1055 4022 50  0000 R CNN
F 2 "" H 1050 4150 50  0001 C CNN
F 3 "" H 1050 4150 50  0001 C CNN
	1    1050 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	1850 4300 1600 4300
Wire Wire Line
	1600 4300 1600 4150
Connection ~ 1600 4150
Wire Wire Line
	2150 4300 2200 4300
Wire Wire Line
	2200 4300 2200 3950
$Comp
L Connector:AudioJack2 J?
U 1 1 5E6F27D0
P 850 3850
F 0 "J?" H 882 4175 50  0000 C CNN
F 1 "AudioJack2" H 882 4084 50  0000 C CNN
F 2 "" H 850 3850 50  0001 C CNN
F 3 "~" H 850 3850 50  0001 C CNN
	1    850  3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 4150 1100 4150
Wire Wire Line
	1050 3750 1100 3750
Wire Wire Line
	1100 3750 1100 4150
Connection ~ 1100 4150
Wire Wire Line
	1100 4150 1150 4150
Wire Wire Line
	1050 3850 1150 3850
$Comp
L power:+3.3V #PWR?
U 1 1 5E6FC1CB
P 1050 3500
F 0 "#PWR?" H 1050 3350 50  0001 C CNN
F 1 "+3.3V" H 1065 3673 50  0000 C CNN
F 2 "" H 1050 3500 50  0001 C CNN
F 3 "" H 1050 3500 50  0001 C CNN
	1    1050 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 3500 1050 3550
Wire Wire Line
	1050 3550 1150 3550
Wire Wire Line
	2200 3950 2250 3950
Connection ~ 2200 3950
Wire Wire Line
	1450 3550 1500 3550
Wire Wire Line
	1500 3550 1500 3850
Connection ~ 1500 3850
Wire Wire Line
	1500 3850 1450 3850
$Comp
L Device:R R?
U 1 1 5E708CEC
P 2400 3950
F 0 "R?" V 2193 3950 50  0000 C CNN
F 1 "8.2k" V 2284 3950 50  0000 C CNN
F 2 "" V 2330 3950 50  0001 C CNN
F 3 "~" H 2400 3950 50  0001 C CNN
	1    2400 3950
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E709A44
P 2550 4100
F 0 "R?" H 2620 4146 50  0000 L CNN
F 1 "2.2k" H 2620 4055 50  0000 L CNN
F 2 "" V 2480 4100 50  0001 C CNN
F 3 "~" H 2550 4100 50  0001 C CNN
	1    2550 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E709D39
P 2550 4250
F 0 "#PWR?" H 2550 4000 50  0001 C CNN
F 1 "GND" H 2555 4077 50  0000 C CNN
F 2 "" H 2550 4250 50  0001 C CNN
F 3 "" H 2550 4250 50  0001 C CNN
	1    2550 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3950 2550 3950
Connection ~ 2550 3950
Text Label 2650 3950 0    50   ~ 0
CV_IN
Text Notes 800  3200 0    50   ~ 0
cv in
Text Notes 8150 4000 0    50   ~ 0
osc1 fine tuning
$Comp
L power:+3.3V #PWR?
U 1 1 5E7174B2
P 8400 4250
F 0 "#PWR?" H 8400 4100 50  0001 C CNN
F 1 "+3.3V" H 8415 4423 50  0000 C CNN
F 2 "" H 8400 4250 50  0001 C CNN
F 3 "" H 8400 4250 50  0001 C CNN
	1    8400 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E718DA6
P 8400 4400
F 0 "R?" H 8470 4446 50  0000 L CNN
F 1 "27k" H 8470 4355 50  0000 L CNN
F 2 "" V 8330 4400 50  0001 C CNN
F 3 "~" H 8400 4400 50  0001 C CNN
	1    8400 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV?
U 1 1 5E719493
P 8400 4700
F 0 "RV?" H 8331 4746 50  0000 R CNN
F 1 "10k" H 8331 4655 50  0000 R CNN
F 2 "" H 8400 4700 50  0001 C CNN
F 3 "~" H 8400 4700 50  0001 C CNN
	1    8400 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E71A635
P 8400 4850
F 0 "#PWR?" H 8400 4600 50  0001 C CNN
F 1 "GND" H 8405 4677 50  0000 C CNN
F 2 "" H 8400 4850 50  0001 C CNN
F 3 "" H 8400 4850 50  0001 C CNN
	1    8400 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 4700 8650 4700
Text Label 8650 4700 0    50   ~ 0
OSC1_FINE_IN
Text Notes 8150 5250 0    50   ~ 0
osc2 fine tuning
$Comp
L power:+3.3V #PWR?
U 1 1 5E72179F
P 8400 5500
F 0 "#PWR?" H 8400 5350 50  0001 C CNN
F 1 "+3.3V" H 8415 5673 50  0000 C CNN
F 2 "" H 8400 5500 50  0001 C CNN
F 3 "" H 8400 5500 50  0001 C CNN
	1    8400 5500
	1    0    0    -1  
$EndComp
Text Notes 700  2350 0    50   ~ 0
gate in
$Comp
L power:GND #PWR?
U 1 1 5E734B38
P 1200 2600
F 0 "#PWR?" H 1200 2350 50  0001 C CNN
F 1 "GND" H 1205 2427 50  0000 C CNN
F 2 "" H 1200 2600 50  0001 C CNN
F 3 "" H 1200 2600 50  0001 C CNN
	1    1200 2600
	-1   0    0    1   
$EndComp
Text Notes 6400 700  0    50   ~ 0
mixer
$Comp
L Amplifier_Operational:LM358 U?
U 2 1 5E75ADC3
P 8200 1400
F 0 "U?" H 8200 1767 50  0000 C CNN
F 1 "LM358" H 8200 1676 50  0000 C CNN
F 2 "" H 8200 1400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 8200 1400 50  0001 C CNN
	2    8200 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E760006
P 8250 1850
F 0 "R?" V 8043 1850 50  0000 C CNN
F 1 "10k" V 8134 1850 50  0000 C CNN
F 2 "" V 8180 1850 50  0001 C CNN
F 3 "~" H 8250 1850 50  0001 C CNN
	1    8250 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 1850 8500 1850
Wire Wire Line
	8500 1850 8500 1400
$Comp
L power:GND #PWR?
U 1 1 5E76282A
P 7900 1300
F 0 "#PWR?" H 7900 1050 50  0001 C CNN
F 1 "GND" V 7905 1172 50  0000 R CNN
F 2 "" H 7900 1300 50  0001 C CNN
F 3 "" H 7900 1300 50  0001 C CNN
	1    7900 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	7900 1500 7900 1850
Wire Wire Line
	7900 1850 8100 1850
$Comp
L Device:R R?
U 1 1 5E768198
P 7400 1100
F 0 "R?" V 7193 1100 50  0000 C CNN
F 1 "10k" V 7284 1100 50  0000 C CNN
F 2 "" V 7330 1100 50  0001 C CNN
F 3 "~" H 7400 1100 50  0001 C CNN
	1    7400 1100
	0    1    1    0   
$EndComp
$Comp
L Device:R_POT RV?
U 1 1 5E76914D
P 6800 1100
F 0 "RV?" H 6731 1146 50  0000 R CNN
F 1 "10k" H 6731 1055 50  0000 R CNN
F 2 "" H 6800 1100 50  0001 C CNN
F 3 "~" H 6800 1100 50  0001 C CNN
	1    6800 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E769E68
P 7400 1800
F 0 "R?" V 7193 1800 50  0000 C CNN
F 1 "10k" V 7284 1800 50  0000 C CNN
F 2 "" V 7330 1800 50  0001 C CNN
F 3 "~" H 7400 1800 50  0001 C CNN
	1    7400 1800
	0    1    1    0   
$EndComp
$Comp
L Device:R_POT RV?
U 1 1 5E769E72
P 6800 1800
F 0 "RV?" H 6731 1846 50  0000 R CNN
F 1 "10k" H 6731 1755 50  0000 R CNN
F 2 "" H 6800 1800 50  0001 C CNN
F 3 "~" H 6800 1800 50  0001 C CNN
	1    6800 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 950  6800 900 
Wire Wire Line
	6800 900  6450 900 
Wire Wire Line
	6800 1650 6800 1600
Wire Wire Line
	6800 1600 6450 1600
$Comp
L power:GND #PWR?
U 1 1 5E773108
P 6800 1250
F 0 "#PWR?" H 6800 1000 50  0001 C CNN
F 1 "GND" H 6805 1077 50  0000 C CNN
F 2 "" H 6800 1250 50  0001 C CNN
F 3 "" H 6800 1250 50  0001 C CNN
	1    6800 1250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E773C47
P 6800 1950
F 0 "#PWR?" H 6800 1700 50  0001 C CNN
F 1 "GND" H 6805 1777 50  0000 C CNN
F 2 "" H 6800 1950 50  0001 C CNN
F 3 "" H 6800 1950 50  0001 C CNN
	1    6800 1950
	1    0    0    -1  
$EndComp
Text Label 6450 900  0    50   ~ 0
OSC1_OUT
Text Label 6450 1600 0    50   ~ 0
OSC2_OUT
Wire Wire Line
	7550 1100 7550 1500
Wire Wire Line
	7900 1500 7550 1500
Connection ~ 7900 1500
Connection ~ 7550 1500
Wire Wire Line
	7550 1500 7550 1800
Connection ~ 8500 1400
Text Notes 3350 650  0    50   ~ 0
opamp power supply
$Comp
L power:+3.3V #PWR?
U 1 1 5E79CA49
P 3750 950
F 0 "#PWR?" H 3750 800 50  0001 C CNN
F 1 "+3.3V" H 3765 1123 50  0000 C CNN
F 2 "" H 3750 950 50  0001 C CNN
F 3 "" H 3750 950 50  0001 C CNN
	1    3750 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4500 5950 4500
Text Label 5950 4500 0    50   ~ 0
OSC1_OUT
Wire Wire Line
	5850 4600 5950 4600
Text Label 5950 4600 0    50   ~ 0
OSC2_OUT
Text Notes 6550 4500 0    50   ~ 0
DAC_1
Text Notes 6550 4600 0    50   ~ 0
DAC_2
Wire Wire Line
	5850 4800 5950 4800
Text Label 5950 4800 0    50   ~ 0
CV_IN
Text Notes 6550 4800 0    50   ~ 0
ADC1_CH4
Text Notes 6550 4900 0    50   ~ 0
ADC1_CH5
Wire Wire Line
	5850 4900 5950 4900
Text Notes 6550 5000 0    50   ~ 0
ADC1_CH6
Wire Wire Line
	5850 5000 5950 5000
Text Label 5950 4900 0    50   ~ 0
OSC1_FINE_IN
Text Label 5950 5000 0    50   ~ 0
OSC2_FINE_IN
Wire Wire Line
	5850 4400 5950 4400
Wire Wire Line
	5850 4300 5950 4300
Text Label 5950 4300 0    50   ~ 0
OSC1_BUTTON
Text Label 5950 4400 0    50   ~ 0
OSC2_BUTTON
Text Notes 6550 4300 0    50   ~ 0
GPIO22
Text Notes 6550 4400 0    50   ~ 0
GPIO23
Wire Wire Line
	5850 4200 5950 4200
Text Notes 6550 4200 0    50   ~ 0
GPIO21
Text Label 5950 4200 0    50   ~ 0
GATE_IN
Connection ~ 1350 2600
Connection ~ 1200 2600
Wire Wire Line
	1350 2600 1200 2600
Wire Wire Line
	1700 2600 1350 2600
Connection ~ 1200 6900
Wire Wire Line
	1200 7350 1200 7300
$Comp
L power:GND #PWR?
U 1 1 5E79BEE2
P 3750 1550
F 0 "#PWR?" H 3750 1300 50  0001 C CNN
F 1 "GND" H 3755 1377 50  0000 C CNN
F 2 "" H 3750 1550 50  0001 C CNN
F 3 "" H 3750 1550 50  0001 C CNN
	1    3750 1550
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM358 U?
U 3 1 5E796493
P 3850 1250
F 0 "U?" H 3808 1296 50  0000 L CNN
F 1 "LM358" H 3808 1205 50  0000 L CNN
F 2 "" H 3850 1250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 3850 1250 50  0001 C CNN
	3    3850 1250
	1    0    0    -1  
$EndComp
Text Label 2000 2900 0    50   ~ 0
GATE_IN
Connection ~ 1700 2900
Wire Wire Line
	1350 2900 1700 2900
Connection ~ 1350 2900
Wire Wire Line
	1100 2900 1350 2900
Wire Wire Line
	1200 2800 1200 2600
Wire Wire Line
	1100 2800 1200 2800
$Comp
L Device:C C?
U 1 1 5E7339AD
P 1700 2750
F 0 "C?" H 1815 2796 50  0000 L CNN
F 1 "100n" H 1815 2705 50  0000 L CNN
F 2 "" H 1738 2600 50  0001 C CNN
F 3 "~" H 1700 2750 50  0001 C CNN
	1    1700 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E732A4E
P 1350 2750
F 0 "R?" H 1420 2796 50  0000 L CNN
F 1 "100k" H 1420 2705 50  0000 L CNN
F 2 "" V 1280 2750 50  0001 C CNN
F 3 "~" H 1350 2750 50  0001 C CNN
	1    1350 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:AudioJack2 J?
U 1 1 5E731C3D
P 900 2900
F 0 "J?" H 932 3225 50  0000 C CNN
F 1 "AudioJack2" H 932 3134 50  0000 C CNN
F 2 "" H 900 2900 50  0001 C CNN
F 3 "~" H 900 2900 50  0001 C CNN
	1    900  2900
	1    0    0    -1  
$EndComp
Text Label 8650 5950 0    50   ~ 0
OSC2_FINE_IN
Wire Wire Line
	8550 5950 8650 5950
$Comp
L power:GND #PWR?
U 1 1 5E7217BD
P 8400 6100
F 0 "#PWR?" H 8400 5850 50  0001 C CNN
F 1 "GND" H 8405 5927 50  0000 C CNN
F 2 "" H 8400 6100 50  0001 C CNN
F 3 "" H 8400 6100 50  0001 C CNN
	1    8400 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV?
U 1 1 5E7217B3
P 8400 5950
F 0 "RV?" H 8331 5996 50  0000 R CNN
F 1 "10k" H 8331 5905 50  0000 R CNN
F 2 "" H 8400 5950 50  0001 C CNN
F 3 "~" H 8400 5950 50  0001 C CNN
	1    8400 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E7217A9
P 8400 5650
F 0 "R?" H 8470 5696 50  0000 L CNN
F 1 "27k" H 8470 5605 50  0000 L CNN
F 2 "" V 8330 5650 50  0001 C CNN
F 3 "~" H 8400 5650 50  0001 C CNN
	1    8400 5650
	1    0    0    -1  
$EndComp
Text Label 1500 6900 0    50   ~ 0
OSC2_BUTTON
Wire Wire Line
	1200 6900 1500 6900
$Comp
L power:GND #PWR?
U 1 1 5E6D6AAE
P 1200 7350
F 0 "#PWR?" H 1200 7100 50  0001 C CNN
F 1 "GND" H 1205 7177 50  0000 C CNN
F 2 "" H 1200 7350 50  0001 C CNN
F 3 "" H 1200 7350 50  0001 C CNN
	1    1200 7350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6D6A96
P 1200 7150
F 0 "C?" H 1315 7196 50  0000 L CNN
F 1 "100n" H 1315 7105 50  0000 L CNN
F 2 "" H 1238 7000 50  0001 C CNN
F 3 "~" H 1200 7150 50  0001 C CNN
	1    1200 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 1850 9300 1500
$Comp
L power:GND #PWR?
U 1 1 5E701E28
P 9300 1850
F 0 "#PWR?" H 9300 1600 50  0001 C CNN
F 1 "GND" H 9305 1677 50  0000 C CNN
F 2 "" H 9300 1850 50  0001 C CNN
F 3 "" H 9300 1850 50  0001 C CNN
	1    9300 1850
	1    0    0    -1  
$EndComp
$Comp
L Connector:AudioJack2 J?
U 1 1 5E700874
P 9600 1400
F 0 "J?" H 9420 1383 50  0000 R CNN
F 1 "AudioJack2" H 9420 1474 50  0000 R CNN
F 2 "" H 9600 1400 50  0001 C CNN
F 3 "~" H 9600 1400 50  0001 C CNN
	1    9600 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	9000 1850 9000 1800
$Comp
L power:GND #PWR?
U 1 1 5E655DAF
P 9000 1850
F 0 "#PWR?" H 9000 1600 50  0001 C CNN
F 1 "GND" H 9005 1677 50  0000 C CNN
F 2 "" H 9000 1850 50  0001 C CNN
F 3 "" H 9000 1850 50  0001 C CNN
	1    9000 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E65548B
P 9000 1650
F 0 "C?" H 8885 1604 50  0000 R CNN
F 1 "100n" H 8885 1695 50  0000 R CNN
F 2 "" H 9038 1500 50  0001 C CNN
F 3 "~" H 9000 1650 50  0001 C CNN
	1    9000 1650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6A3B12
P 8650 1650
F 0 "R?" H 8720 1696 50  0000 L CNN
F 1 "100k" H 8720 1605 50  0000 L CNN
F 2 "" V 8580 1650 50  0001 C CNN
F 3 "~" H 8650 1650 50  0001 C CNN
	1    8650 1650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E6A457D
P 8650 1850
F 0 "#PWR?" H 8650 1600 50  0001 C CNN
F 1 "GND" H 8655 1677 50  0000 C CNN
F 2 "" H 8650 1850 50  0001 C CNN
F 3 "" H 8650 1850 50  0001 C CNN
	1    8650 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 1850 8650 1800
Wire Wire Line
	8650 1500 8650 1400
Wire Wire Line
	8650 1400 9000 1400
Wire Wire Line
	1700 2900 2000 2900
$Comp
L Device:C C?
U 1 1 5E6BFC24
P 7100 1100
F 0 "C?" V 6848 1100 50  0000 C CNN
F 1 "100n" V 6939 1100 50  0000 C CNN
F 2 "" H 7138 950 50  0001 C CNN
F 3 "~" H 7100 1100 50  0001 C CNN
	1    7100 1100
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5E6C0B85
P 7100 1800
F 0 "C?" V 6848 1800 50  0000 C CNN
F 1 "100n" V 6939 1800 50  0000 C CNN
F 2 "" H 7138 1650 50  0001 C CNN
F 3 "~" H 7100 1800 50  0001 C CNN
	1    7100 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	8500 1400 8650 1400
Connection ~ 8650 1400
Connection ~ 7550 1800
$Comp
L power:GND #PWR?
U 1 1 5E770DB2
P 7550 2100
F 0 "#PWR?" H 7550 1850 50  0001 C CNN
F 1 "GND" H 7555 1927 50  0000 C CNN
F 2 "" H 7550 2100 50  0001 C CNN
F 3 "" H 7550 2100 50  0001 C CNN
	1    7550 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E770060
P 7550 1950
F 0 "C?" H 7665 1996 50  0000 L CNN
F 1 "1n" H 7665 1905 50  0000 L CNN
F 2 "" H 7588 1800 50  0001 C CNN
F 3 "~" H 7550 1950 50  0001 C CNN
	1    7550 1950
	1    0    0    -1  
$EndComp
NoConn ~ 4650 3000
NoConn ~ 4650 3100
$Comp
L Device:C C?
U 1 1 5E7940EE
P 5500 2250
F 0 "C?" V 5248 2250 50  0000 C CNN
F 1 "100n" V 5339 2250 50  0000 C CNN
F 2 "" H 5538 2100 50  0001 C CNN
F 3 "~" H 5500 2250 50  0001 C CNN
	1    5500 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	5250 2600 5250 2250
Wire Wire Line
	5250 2250 5350 2250
Connection ~ 5250 2250
Wire Wire Line
	5250 2250 5250 2200
$Comp
L power:GND #PWR?
U 1 1 5E7BAA71
P 4200 3150
F 0 "#PWR?" H 4200 2900 50  0001 C CNN
F 1 "GND" H 4205 2977 50  0000 C CNN
F 2 "" H 4200 3150 50  0001 C CNN
F 3 "" H 4200 3150 50  0001 C CNN
	1    4200 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E7BB5D0
P 4200 3000
F 0 "C?" H 4315 3046 50  0000 L CNN
F 1 "100n" H 4315 2955 50  0000 L CNN
F 2 "" H 4238 2850 50  0001 C CNN
F 3 "~" H 4200 3000 50  0001 C CNN
	1    4200 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E7BC975
P 4200 2600
F 0 "R?" H 4270 2646 50  0000 L CNN
F 1 "10k" H 4270 2555 50  0000 L CNN
F 2 "" V 4130 2600 50  0001 C CNN
F 3 "~" H 4200 2600 50  0001 C CNN
	1    4200 2600
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E7BD58B
P 4200 2450
F 0 "#PWR?" H 4200 2300 50  0001 C CNN
F 1 "+3.3V" H 4215 2623 50  0000 C CNN
F 2 "" H 4200 2450 50  0001 C CNN
F 3 "" H 4200 2450 50  0001 C CNN
	1    4200 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2750 4200 2800
Wire Wire Line
	4200 2800 4650 2800
Connection ~ 4200 2800
Wire Wire Line
	4200 2800 4200 2850
Text Label 3900 2800 0    50   ~ 0
ESP_EN
Wire Wire Line
	3900 2800 4200 2800
$Comp
L Device:R R?
U 1 1 5E7CC781
P 6200 2900
F 0 "R?" V 5993 2900 50  0000 C CNN
F 1 "499" V 6084 2900 50  0000 C CNN
F 2 "" V 6130 2900 50  0001 C CNN
F 3 "~" H 6200 2900 50  0001 C CNN
	1    6200 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 2900 6050 2900
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5E7D1088
P 5000 6550
F 0 "J?" H 5080 6542 50  0000 L CNN
F 1 "UART DOWNLOAD" H 5080 6451 50  0000 L CNN
F 2 "" H 5000 6550 50  0001 C CNN
F 3 "~" H 5000 6550 50  0001 C CNN
	1    5000 6550
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 5E7D422D
P 4800 7000
F 0 "SW?" V 4754 7098 50  0000 L CNN
F 1 "SW_SPST" V 4845 7098 50  0000 L CNN
F 2 "" H 4800 7000 50  0001 C CNN
F 3 "~" H 4800 7000 50  0001 C CNN
	1    4800 7000
	0    1    1    0   
$EndComp
Text Label 4500 6800 0    50   ~ 0
ESP_EN
Wire Wire Line
	4800 6800 4800 6750
Wire Wire Line
	4500 6800 4800 6800
Connection ~ 4800 6800
$Comp
L power:GND #PWR?
U 1 1 5E7E1C85
P 4800 7200
F 0 "#PWR?" H 4800 6950 50  0001 C CNN
F 1 "GND" H 4805 7027 50  0000 C CNN
F 2 "" H 4800 7200 50  0001 C CNN
F 3 "" H 4800 7200 50  0001 C CNN
	1    4800 7200
	1    0    0    -1  
$EndComp
Text Notes 8200 2550 0    50   ~ 0
lcd screen
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5E778D06
P 9000 2950
F 0 "J?" H 9080 2942 50  0000 L CNN
F 1 "Conn_01x04" H 9080 2851 50  0000 L CNN
F 2 "" H 9000 2950 50  0001 C CNN
F 3 "~" H 9000 2950 50  0001 C CNN
	1    9000 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E779CB1
P 8750 2850
F 0 "#PWR?" H 8750 2600 50  0001 C CNN
F 1 "GND" H 8755 2677 50  0000 C CNN
F 2 "" H 8750 2850 50  0001 C CNN
F 3 "" H 8750 2850 50  0001 C CNN
	1    8750 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	8750 2850 8800 2850
$Comp
L power:+5V #PWR?
U 1 1 5E77E2F0
P 8500 2850
F 0 "#PWR?" H 8500 2700 50  0001 C CNN
F 1 "+5V" H 8515 3023 50  0000 C CNN
F 2 "" H 8500 2850 50  0001 C CNN
F 3 "" H 8500 2850 50  0001 C CNN
	1    8500 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E77F893
P 8050 2950
F 0 "C?" V 7798 2950 50  0000 C CNN
F 1 "100n" V 7889 2950 50  0000 C CNN
F 2 "" H 8088 2800 50  0001 C CNN
F 3 "~" H 8050 2950 50  0001 C CNN
	1    8050 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 2950 8500 2950
Wire Wire Line
	8500 2950 8500 2850
Wire Wire Line
	8500 2950 8800 2950
Connection ~ 8500 2950
$Comp
L power:GND #PWR?
U 1 1 5E78964D
P 7900 2950
F 0 "#PWR?" H 7900 2700 50  0001 C CNN
F 1 "GND" V 7905 2822 50  0000 R CNN
F 2 "" H 7900 2950 50  0001 C CNN
F 3 "" H 7900 2950 50  0001 C CNN
	1    7900 2950
	0    1    1    0   
$EndComp
Text Label 8100 3050 0    50   ~ 0
LCD_SDA
Text Label 8100 3150 0    50   ~ 0
LCD_SCL
Wire Wire Line
	5850 3200 5950 3200
Wire Wire Line
	5850 3300 5950 3300
Text Label 5950 3200 0    50   ~ 0
LCD_SDA
Text Label 5950 3300 0    50   ~ 0
LCD_SCL
Wire Wire Line
	8100 3050 8450 3050
$Comp
L Device:R R?
U 1 1 5E79FA65
P 8450 3350
F 0 "R?" H 8520 3396 50  0000 L CNN
F 1 "100k" H 8520 3305 50  0000 L CNN
F 2 "" V 8380 3350 50  0001 C CNN
F 3 "~" H 8450 3350 50  0001 C CNN
	1    8450 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E7A14EE
P 8800 3350
F 0 "R?" H 8870 3396 50  0000 L CNN
F 1 "100k" H 8870 3305 50  0000 L CNN
F 2 "" V 8730 3350 50  0001 C CNN
F 3 "~" H 8800 3350 50  0001 C CNN
	1    8800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3200 8800 3150
Connection ~ 8800 3150
Wire Wire Line
	8450 3200 8450 3050
Connection ~ 8450 3050
Wire Wire Line
	8450 3050 8800 3050
Wire Wire Line
	8100 3150 8800 3150
$Comp
L power:GND #PWR?
U 1 1 5E7BC667
P 8450 3500
F 0 "#PWR?" H 8450 3250 50  0001 C CNN
F 1 "GND" H 8455 3327 50  0000 C CNN
F 2 "" H 8450 3500 50  0001 C CNN
F 3 "" H 8450 3500 50  0001 C CNN
	1    8450 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E7BD36C
P 8800 3500
F 0 "#PWR?" H 8800 3250 50  0001 C CNN
F 1 "GND" H 8805 3327 50  0000 C CNN
F 2 "" H 8800 3500 50  0001 C CNN
F 3 "" H 8800 3500 50  0001 C CNN
	1    8800 3500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
